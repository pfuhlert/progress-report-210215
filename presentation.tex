\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage{animate}
\usepackage[backend=biber, style=apa, citestyle=authoryear]{biblatex}
\addbibresource{../../mendeley/library.bib}
\AtBeginBibliography{\scriptsize} % print bibliography small

\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{romannum}
\usepackage{siunitx}
\usepackage{tikz}
\usetikzlibrary{arrows.meta, fit, calc, matrix,arrows,positioning, chains, decorations.pathreplacing}

\usepackage{tikzscale}
\usepackage{standalone}

\usepackage{pgfplots}
\usepackage[sfdefault]{Fira Sans}
\usepackage[mathrm=sym]{unicode-math}
\usefonttheme{professionalfonts}
\setmathfont{Fira Math}
% ### Experimental ###

% \includeonlyframes{current}
% ### End Experimental ###

% ### Theme ###

\usetheme{metropolis}
\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}
\title{Deep Recurrent Survival Analysis for Prostate Cancer Patients}
\date{15.02.2021}
\author{Patrick Fuhlert}
\institute{Institute of Medical Systems Biology\\
Center for Molecular Neurobiology Hamburg\\
University Medical Center Hamburg-Eppendorf}
\makeatletter
\input{metropolis_extensions.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle

\begin{frame}[allowframebreaks]{Content}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{Introduction}

\subsection{Prostate Cancer}

\begin{frame}{Prostate Cancer (PCa)}

  \begin{columns}
    \begin{column}{0.6\textwidth}

      Worldwide statistics\footnotemark:
      \begin{itemize}
        \item second most common type of cancer in men
        \item fifth-leading cause of cancer-related death in men
        \item more than 1 million new cases per year
        \item up to 370,000 deaths per year
        \item Common treatment: Radical Prostatectomy
        \item Common side-effects of prostatectomy: urinary incontinence, erectile dysfunction
      \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{img/pros_cancer.png}
        {\footnotesize
          [Cancer Research UK]}
      \end{figure}

    \end{column}
  \end{columns}
  \footnotetext{\tiny{https://www.wcrf.org/dietandcancer/cancer-trends/prostate-cancer-statistics}}
\end{frame}

\begin{frame}{Prostate Cancer}
  \begin{figure}[]
    \centering
    \includegraphics[width=.75\textwidth]{img/pca-lifecycle.png}
    \caption{\scriptsize{Prostate Cancer Patient Lifecycle. AS=Active Surveillance, RT=Radiotherapy, RP=Radical Prostatectomy}}
  \end{figure}
  \begin{enumerate}
    \pause{}
    \item Is a biopsy necessary?
    \item What primary treatment should be applied?
    \item \alert{How long does a patient remain progression free?}
    \item Is additional treatment after RP necessary?
  \end{enumerate}
\end{frame}

\subsection{Survival Analysis}

\begin{frame}{Overview}
  Survival analysis tries to find the expected duration of time until one or more events happen.
  \hspace*\fill{\small--- Miller et al. 1997}
  \vspace{1em}

  \begin{description}[leftmargin=1em, labelwidth=2em]
    \item[Medical Survival] How long till patient dies?
    \item[Reliability Theory] How long until something breaks?
    \item[Economics] How long until a customer withdraws?
  \end{description}
\end{frame}

\begin{frame}{Censoring}
  \begin{figure}
    \centering
    \includestandalone[height=.4\textheight]{tikz/censoring}
    \small
    \caption{Censoring over time}
  \end{figure}

  \begin{itemize}
    \small
    \item Typical for medical studies, not all included patients participate a study from start to finish
    \item If they only participate in parts of a study, their data is considered \alert{censored}
    \item We mostly deal with right-censored data (all patients joined at a certain time, but not all completed)
  \end{itemize}
\end{frame}

\section{Theory}

\begin{frame}{Survival Curve}
  \begin{itemize}
    \item How does a population survive over time?
    \item Look at \alert{relative frequencies} at each event time
    \item Kaplan-Meier Estimator predicts \(\hat{S}(t)\) that approximates real survival curve \(S(t)\)
    \item Estimate the survival rate of a population by relative frequencies:
  \end{itemize}
  \begin{columns}
    \hfill
    \begin{column}{0.2\textwidth}
      \[ \hat{S}(t) = \prod_{i: t_i \leq t} \left( 1 - \frac{d_i}{n_i} \right) \]
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{description}[labelwidth=2cm]
        \scriptsize
        \item[$t_i$] time where at least one event happened
        \item[$d_i$] number of events at $t_i$
        \item[$n_i$] number of patients that survived $t_i$
      \end{description}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Survival Curve --- Example}
  \begin{figure}
    \includegraphics[width=\textwidth]{img/survival.png}
  \end{figure}
\end{frame}

\subsection{Stratification}

\begin{frame}{Stratification}
  \begin{itemize}
    \item Gives overview of observed population survival curve
    \item What about the influence of a covariate?
    \item Naive approach: Calculate Survival Group for subpopulations
    \item Needs appropriate (sub-) population size for each case
    \item Stratification covariates might not be independent of other covariates
  \end{itemize}
\end{frame}

\begin{frame}{Toy Dataset --- Survival Curve}
  \begin{minipage}{.4\textwidth}
    \begin{table}
      \small
      \begin{tabular}{lll|r}
        \alert<3>{$x_0$} & \alert<4>{$x_1$} & \alert<5>{$x_2$} & $t$  \\
        \midrule
        \alert<3>{0}     & \alert<4>{0}     & \alert<5>{0}     & 1000 \\
        \alert<3>{1}     & \alert<4>{0}     & \alert<5>{0}     & 900  \\
        \alert<3>{0}     & \alert<4>{1}     & \alert<5>{0}     & 800  \\
        \alert<3>{1}     & \alert<4>{1}     & \alert<5>{0}     & 700  \\
        \alert<3>{0}     & \alert<4>{0}     & \alert<5>{1}     & 600  \\
        \alert<3>{1}     & \alert<4>{0}     & \alert<5>{1}     & 500  \\
        \alert<3>{0}     & \alert<4>{1}     & \alert<5>{1}     & 400  \\
        \alert<3>{1}     & \alert<4>{1}     & \alert<5>{1}     & 300  \\
      \end{tabular}
    \end{table}
    \[t^{i} = 1000 - 100 x_0^{i} - 200 x_1^{i} - 400 x_2^{i}\]
  \end{minipage}
  \pause
  \begin{minipage}{.59\textwidth}
    \begin{figure}
      \includegraphics<2>[width=.8\textwidth]{img/toy_km.png}
      \includegraphics<3>[width=.8\textwidth]{img/toy_km_strat_x0.png}
      \includegraphics<4>[width=.8\textwidth]{img/toy_km_strat_x1.png}
      \includegraphics<5>[width=.8\textwidth]{img/toy_km_strat_x2.png}
    \end{figure}
  \end{minipage}
\end{frame}

\subsection{Survival Models}

\begin{frame}{Cox Proportional Hazards Model [\cite{Cox1972}] I}
  \begin{itemize}
    \item Consider \(h(t) = 1 - S(t)\) to be called the hazard function
    \item Split hazard function in base hazard and influence of covariates
          \[h(t) = h_0(t) \cdot s\]
    \item \(h_0(t)\) is called the \alert{baseline hazard}. It corresponds to the hazard if all covariates are \(0\)
    \item Proportional Hazard Assumption:
    \item[] All individuals have the \alert{same hazard function}, but a \alert{unique scaling factor}
  \end{itemize}
\end{frame}

\begin{frame}{Cox Proportional Hazards Model [\cite{Cox1972}] II}
  \begin{itemize}
    \item Typically assumed that the hazard responds exponentially:
          \[h(t) = h_0(t) \cdot \underbrace{\exp(b_1 x_1 + b_2x_2 + \cdots + b_p x_p)}_{\text{Independent of } t}\]
          \hspace*\fill{\small---~[\cite{Cox1972}]}
    \item coefficients \(b_p\) measure the \alert{impact of covariates}
    \item if \(b_p > 0\) the covariate increases the hazard and thus decreases the length of survival
    \item Obtain \(b_p\) by Multivariate Linear Regression (independent of t)
  \end{itemize}
\end{frame}

\begin{frame}{DeepSurv}
  \begin{itemize}
    \item MLP as input for Cox PH Model
  \end{itemize}
  \begin{figure}[]
    \centering
    \includegraphics[height=.5\textheight]{img/deep_surv_arch.jpg}
    \caption{DeepSurv Architecture (Cox-MLP)}
  \end{figure}
\end{frame}

\begin{frame}{DeepSurv II}
  \begin{itemize}
    \item Simple Neural Network
    \begin{itemize}
      \item NN with 2 hidden layers and 64 nodes each
      \item \(40\% \) dropout
      \item batch normalization
    \end{itemize}
    \item Implemented in pycox\footnote{\href{https://github.com/havakv/pycox}{github.com/havakv/pycox}} package
    \item Only trained on patients that received event
  \end{itemize}
\end{frame}

\begin{frame}{Cox Proportional Hazards Model --- Example}
  \begin{figure}
    \centering
    \includegraphics[width=.9\textwidth]{img/coxph_example.png}
    \caption{Exemplary Cox Proportional Hazards model survival curves}
  \end{figure}
\end{frame}

\subsection{Neural Network Approaches}

\begin{frame}{CoxTime [\cite{Kvamme2019a}]}
  \begin{figure}[]
    \centering
    \mbox{\includestandalone[height=.5\textheight]{tikz/coxtime}}
    \caption{CoxTime Model Architecture}
  \end{figure}
  \begin{itemize}
    \item $h(t|x^{(i)}) = \alert<2>{h_0(t)} \exp(g(t, x^{(i)}))$
    \item Comparable loss to Cox Proportional Hazards
    \item Continuous output
    \item Time dependency allows for "arbitrary" survival curves
  \end{itemize}
\end{frame}

\begin{frame}{CoxTime --- Example}
  \begin{figure}[]
    \centering
    \includegraphics[width=.9\textwidth]{img/toy_coxtime.png}
    \caption{Exemplary CoxTime survival curves}
  \end{figure}
\end{frame}

\begin{frame}{CoxTime --- Example II}
  \begin{figure}[]
    \centering
    \includegraphics[width=.9\textwidth]{img/coxtime_example.png}
    \caption{Exemplary CoxTime survival curves}
  \end{figure}
\end{frame}

\section{Our Current Approach}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Sequential output can be modeled by RNN architecture
    \item[] The survival curve $S(t)$ depends on $S(t - \Delta t)$
    \item This implies output discretization
    \item Combines discrete survival RNN ideas from DRSA [\cite{Ren2018}] and RNN-Surv[\cite{Giunchiglia2018}]
  \end{itemize}
\end{frame}

\begin{frame}{Discrete Hazard Rates}
  \begin{figure}
    \includegraphics[height=.2\textheight]{tikz/output_grid}
    \caption{Discrete output grid notation}
  \end{figure}
  \begin{itemize}
    \item Divide Survival Curve into $L$ buckets
    \item Use hazard rate instead of survival rate [\cite{Ren2018}]
    \item Hazard $h_l = Pr(t \in V_l | t > t_{l-1})$
    \item<2> Survival Curve \[S(t_l|x^{(i)}) = \prod_{1}^{l} \left( 1 - h_l^{(i)}\right)\]
  \end{itemize}

\end{frame}

\begin{frame}{Discrete Hazard Rates --- Visualization}
  \begin{figure}
    \centering
    \includegraphics[width=.6\textwidth]{img/drsa_rnn_example.png}
    \caption{Exemplary survival curve of our approach}
  \end{figure}
  \[\text{Survival Curve } S(t_l|x^{(i)}) = \prod_{1}^{l} \left( 1 - h_l^{(i)}\right)\]
\end{frame}

\begin{frame}{Loss [\cite{Ren2018}]}
  \begin{figure}
    \centering
    \includegraphics[width=.6\textwidth]{img/drsa_rnn_example.png}
    \caption{Exemplary survival curve of our approach}
  \end{figure}
  \begin{description}[labelwidth=2cm]
    \small
    \item[uncensored] "push down" hazard rates before event, "pull up" hazard rate at event
    \item[censored] "push down" hazard rates before and at censoring event
  \end{description}
\end{frame}

\begin{frame}{Our Architecture}
  \begin{minipage}{.8\textwidth}
    \begin{figure}
      \centering
      \includestandalone[height=.8\textheight]{tikz/model_rnn_drsa}
    \end{figure}
  \end{minipage}
  \hfill
  \begin{minipage}{.18\textwidth}
    \begin{figure}
      \includestandalone[height=.8\textheight]{tikz/model_rnn_drsa_desc}
    \end{figure}
  \end{minipage}
\end{frame}

\begin{frame}{Hyperparameters}
  \begin{description}[labelwidth=2cm]
    \small
    \item[Input Encoder] Nodes \only<2>{\alert{32}}, Layers \only<2>{\alert{2}}
    \item[RNN Decoder] Node \only<2>{\alert{32}}, Layers \only<2>{\alert{1}}, Type (\alert<2>{LSTM}, GRU, \dots)
    \item[Output]  Nodes \only<2>{\alert{1}}, Layers \only<2>{\alert{1}}
    \item[Loss] relative loss weights $\alpha$ \only<2>{\alert{0.5}}, $\beta$ \only<2>{\alert{10}}
  \end{description}
\end{frame}

\begin{frame}{Our Survival Curves}

  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/toy_drsa.png}
    \caption{Survival curves on toy dataset with our approach}
  \end{figure}

\end{frame}

\section{Martini-klinik data}

\begin{frame}{Dataset}
  \begin{figure}
    \centering
    \includestandalone[height=.5\textheight]{tikz/mk_timeline}
    \caption{Event outcomes for MK PCa postOP dataset}
  \end{figure}
  \begin{itemize}
    \item Martini-klinik provided 16,000 patients
    \item All patients that received prostatectomy
    \item Patients are aligned at $t_0 = $ day of prostatectomy
  \end{itemize}
\end{frame}

\begin{frame}{Martini-klinik PCa Survival PostOp Dataset --- Covariates}

  \begin{minipage}{.39\textwidth}
    \scriptsize
    \begin{table}[]
      \begin{tabular}{l|r}
        \textbf{Covariate}         & \textbf{Value Range} \\
        \midrule
        psa\_preop\_days           & $[-730, 0]$          \\
        psa\_level\_preop          & $[0, 1000]$          \\
        \midrule
        gleason\_2\_percent        & $[0, 1]$             \\
        gleason\_3\_percent        & $[0, 1]$             \\
        gleason\_4\_percent        & $[0, 1]$             \\
        gleason\_5\_percent        & $[0, 1]$             \\
        \midrule
        capsular\_invasion         & $\{0, 1\}$           \\
        seminal\_vesicle\_invasion & $\{0, 1\}$           \\
        lymph\_node\_invasion      & $\{0, 1\}$           \\
        margin\_status             & $\{0, 1\}$           \\
        \midrule
        age\_op                    & $[0, 100]$           \\
        bmi                        & $[0, 100]$           \\
        active\_smoker             & $\{0, 1\}$           \\
        \dots                      &                      \\
        \midrule
        adjuvant                   & $\{0, 1\}$           \\
        neoadjuvant                & $\{0, 1\}$           \\
      \end{tabular}
    \end{table}
  \end{minipage}
  \hfill
  \begin{minipage}{.5\textwidth}
    \begin{itemize}
      \small
      \item 16,000 patients that received radical prostatectomy
      \item 78\% censored
      \item median follow up of 3.2 years (1170 days)
      \item 2.4\% missing values
      \item Preprocessing
            \begin{itemize}
              \item Standardize
              \item KNN imputation (mean on continuous, median on categorical)
            \end{itemize}
    \end{itemize}
  \end{minipage}
\end{frame}

\begin{frame}{Martini-klinik PCa Survival PostOp Dataset --- Outcome}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/mk_events.png}
    \caption{MK: Censored and uncensored patient event times}
  \end{figure}
  \small
  Event is defined as any \\
  $\{$biochemical recurrence (PSA), pca related death, additional treatment, metastases\}
\end{frame}

\section{Evaluation}

\subsection{Metrics}

\begin{frame}{Concordance Index}
  \begin{itemize}
    \item Population level metric by \cite{Harrell}
    \item Generalization of the area under the ROC curve for censored data
    \item Fraction of correct order of predictions \(\hat{t}_i\) vs outcome \(t_i\):
          \[\text{c-index = } \displaystyle \frac{\sum_{i, j} 1_{t_j < t_i} \cdot 1_{\hat{t}_j < \hat{t}_i} \cdot \delta_i}{\sum_{i, j} 1_{t_j < t_i} \cdot \delta_i}\]
    \item Leave out a pair if the concordance is unknown due to censoring (\(\delta_i = 1\))
    \item Independent of absolute values, only relative order
    \item Perfect score is \(1\), random prediction is \(\approx 0.5\)
  \end{itemize}
\end{frame}

\begin{frame}{Concordance Index --- Example}
  Suppose there are three survival probabilities at $t_1$: $S(t_1|x_1) > S(t_1|x_2) > S(t_1|x_3)$
  The concordance index rates the \alert{order} of our predictions:

  \begin{figure}
    \includestandalone[height=.6\textheight]{tikz/cindex123}
    \qquad
    \includestandalone[height=.6\textheight]{tikz/cindex312}
    \caption{Concordance Index of 1 and .33}
  \end{figure}

\end{frame}

\begin{frame}{Brier Score}
  \begin{itemize}
    \item Individual level metric
    \item Consider \(p_i = \hat{S}(x_i)\) as the predicted survival and \(o_i\) the observed survival of an individual at a specific time \(t\)
    \item[] \[BS = \frac{1}{N} \displaystyle \sum_{t=1}^{N} {(p_i - o_i)}^2\]
          \hspace*\fill{\small---~[\cite{BrierGlennWandAllen1951}]}
    \item Mean squared error between prediction \(p_i\) and outcome \(o_i\)
    \item Loss Function --- Lower is better
    \item Ranges from \(0\) (perfect estimation) to \(1\)
  \end{itemize}
\end{frame}

\subsection{Quantitative}

\begin{frame}{Scores on Preprocessed Dataset $\pm$ std}
  \begin{table}[]
    \begin{tabular}{l||r|r}
                & concordance-index-td & brier-score-td \\
      \toprule
      CoxPH     & $.812 \pm 0$         & $.12 \pm 0$    \\
      CoxTime   & $.805 \pm .023$      & $.16\pm .01$   \\
      DRSA-LSTM & $.817 \pm .020$      & $.118\pm .01$  \\
      DRSA-MLP  & $.812 \pm .018$      & $.136\pm .02$  \\
    \end{tabular}
  \end{table}
\end{frame}

\subsection{Qualitative}

\begin{frame}{Survival Curves for sample patients --- CoxPH}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/mk_coxph_plot.png}
    \caption{MK dataset: Cox Proportional Hazards survival curves on sample patients}
  \end{figure}
\end{frame}

\begin{frame}{Survival Curves for sample patients --- CoxTime}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/mk_coxtime_plot.png}
    \caption{MK dataset: CoxTime survival curves on sample patients}
  \end{figure}
\end{frame}

\begin{frame}{Survival Curves for sample patients --- Our Model}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/mk_drsa_plot.png}
    \caption{MK dataset: Our model survival curves on sample patients}
  \end{figure}
\end{frame}

\begin{frame}[label=current]{MK Data --- Stratification}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/mk_strat_coxph.png}
    \caption{Cox Proportional Hazards stratified by PSA}
  \end{figure}
\end{frame}

\section{Conclusion}

\begin{frame}{Summary}

  \begin{itemize}
    \item CoxPH only calculates a scaling factor, so all survival curves look alike
    \item CoxTime enables unique survival curves, but does not exploit sequential property
    \item Our approach tries to exploit sequential dependencies
    \item Current metrics do not really help to find the best models
  \end{itemize}
\end{frame}

\begin{frame}{Future Work --- with Anne, Esther, Sebastian, Fabian, Karin}
  \begin{itemize}
    \small
    \item \textbf{Find better Metrics}
          \begin{itemize}
            \item Average negative log probability
            \item Weight loss by Inverse Probability of Censoring Weights (IPCW)
            \item D-calibration
          \end{itemize}
    \item \textbf{Let the model focus more on earlier time steps}
          \begin{itemize}
            \item Day 0 to 250 should be more important than 1000 to 1250
            \item Analyze non-equidistant output grids in discrete case (e.g. log or percentiles)
            \item Weight patients with event higher than censored individuals
            \item Goal: Identify subcohort with fast recurrence
          \end{itemize}
    \item \textbf{More Input}
          \begin{itemize}
            \item Visualize Embedding space
            \item Include time series: e.g. Encoder on PSA value tables
          \end{itemize}
    \item \textbf{Model uncertainty and explainability}
          \begin{itemize}
            \item Ensemble models
            \item Bayesian networks
          \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[allowframebreaks]{References}
  \scriptsize{\printbibliography[heading=none]}
\end{frame}

\backup

\begin{frame}{Cox Proportional Hazards Model}
  \begin{itemize}
    \item Widely used in medical statistics
    \item Consider \(h(t) = 1 - S(t)\) to be called the hazard function
    \item Split hazard function in base hazard and influence of covariates
          \[h(t) = h_0(t) \cdot s\]
    \item \(h_0(t)\) is called the \alert{baseline hazard}. It corresponds to the hazard if all covariates are \(0\)
    \item Proportional Hazards Assumption:
    \item[] All individuals have the \alert{same hazard function}, but a \alert{unique scaling factor}
    \item Covariates assumed \alert{linear and independent}, trained by linear regression
  \end{itemize}

\end{frame}

\begin{frame}{Discrete Hazard Rates}
  \begin{itemize}
    \item $L+1$ time slices $t_0 < t_1 < t_2 < \dots < t_L$
    \item $L$ buckets $V_l = (t_{l-1},\, t_l]$
    \item Hazard $h_l = Pr(t \in V_l | t > t_{l-1})$ is the probability that the event happens in bucket $l$ if it did not happen before
  \end{itemize}

  \begin{figure}
    \includegraphics[height=.2\textheight]{tikz/output_grid}
    \caption{Discrete output grid notation}
  \end{figure}

  \begin{itemize}
    \item Survival Curve \[S(t_l|x^{(i)}) = \prod_{l:l\leq l^{(i)}} \left( 1 - h_l^{(i)}\right)\]
  \end{itemize}
\end{frame}

\begin{frame}{Our Architecture -- MLP version}
  \begin{minipage}{.5\textwidth}
    \begin{itemize}
      \item Replace RNN architecture with additional Dense layer
    \end{itemize}
  \end{minipage}
  \begin{minipage}{.49\textwidth}
    \begin{figure}
      \centering
      \includestandalone[height=.8\textheight]{tikz/model_mlp_drsa}
    \end{figure}
  \end{minipage}
\end{frame}

\begin{frame}{DRSA Loss --- In Detail}

  \begin{itemize}
    \item $\mathbb{D}$ is Labels of patients, so $(x^{(i)}, t^{(i)})$
          \begin{itemize}
            \item $\mathbb{D}_{unc}$ is the uncensored subset
            \item $\mathbb{D}_{cen}$ is the censored subset
          \end{itemize}
    \item $l^{(i)}$ is bucket with actual event
    \item $h^i_{l^{(i)}}$ is hazard rate in bucket with actual event

  \end{itemize}

  \scriptsize{
    \begin{align*}
      L_z     & = & -      & \sum_{(x^{(i)}, t^{(i)}) \in \mathbb{D}_{unc}} \;
      \log h_{l^{(i)}}^{(i)} + \sum_{l:l < l^{(i)}} \log \left( 1 - h_l^{(i)} \right) \\
      L_{unc} & = & -      & \sum_{(x^{(i)}, t^{(i)}) \in \mathbb{D}_{unc}} \;
      \log \left[ 1 - \prod_{l:l \leq l^{(i)}} \left(1 - h_l^{(i)}\right)\right]      \\
      L_{cen} & = & - \log & \sum_{(x^{(i)}, t^{(i)}) \in \mathbb{D}_{cen}} \;
      \sum_{l:l \leq l^{(i)}} \log \left(1 - h_l^{(i)} \right)                        \\
    \end{align*}
  }

  \[L = \alpha L_z + (1 - \alpha) \left( \beta L_{unc} + L_{cen}\right)\]

\end{frame}

\begin{frame}{Concordance Index --- Example}
  Suppose there are three survival probabilities at $t_1$: $S(t_1|x_1) > S(t_1|x_2) > S(t_1|x_3)$
  The concordance index rates the \alert{order} of our predictions:

  \begin{table}[]
    \begin{tabular}{l||r}
      Prediction                                                            & Score            \\
      \toprule
      $\hat{S}(t_1|x_1) > \hat{S}(t_1|x_2) > \hat{S}(t_1|x_3)$              & 1                \\
      \alert<2,4>{$\hat{S}(t_1|x_1) > \hat{S}(t_1|x_3) > \hat{S}(t_1|x_2)$} & \alert<2,4>{.67} \\
      $\hat{S}(t_1|x_2) > \hat{S}(t_1|x_1) > \hat{S}(t_1|x_3)$              & .67              \\
      $\hat{S}(t_1|x_2) > \hat{S}(t_1|x_3) > \hat{S}(t_1|x_1)$              & .33              \\
      $\hat{S}(t_1|x_3) > \hat{S}(t_1|x_1) > \hat{S}(t_1|x_2)$              & .33              \\
      $\hat{S}(t_1|x_3) > \hat{S}(t_1|x_2) > \hat{S}(t_1|x_1)$              & 0                \\
    \end{tabular}
  \end{table}

  \only<1->{If we predict \hphantom{10}$\hat{S}(t_1|x_1) = 400,  \hat{S}(t_1|x_2) = 200, \hat{S}(t_1|x_3) = 300$, } \only<2->{\alert<2>{we score $.67$\\}}
  \only<3->{If we predict $\hat{S}(t_1|x_1) = 10400,  \hat{S}(t_1|x_2) = 200, \hat{S}(t_1|x_3) = 300$, } \only<4->{\alert{we score $.67$}}

\end{frame}


\end{document}
